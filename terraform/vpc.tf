terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "myjavaapp-terraform-state"
    key = "myjavaapp/state.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
    region = var.region
}

/*
locals {
  cluster_name = var.eks_cluster_name 
}


data "aws_availability_zones" "azs" {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.8.1"

  name = "${var.env_prefix}-vpc"
  cidr = var.vpc_cidr_block
  private_subnets = var.private_subnet_cidr_blocks
  public_subnets = var.public_subnet_cidr_blocks
  azs = data.aws_availability_zones.azs.names
  enable_nat_gateway = true
  single_nat_gateway = true 
  enable_dns_hostnames = true 

  tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared" 
  }
  
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb" = 1
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared" 
    "kubernetes.io/role/internal-elb" = 1
  }

}
*/
