/*
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "20.11.1"

  cluster_name = var.eks_cluster_name
  cluster_version = var.k8_version
  cluster_endpoint_public_access = true
  enable_cluster_creator_admin_permissions = true

  subnet_ids = module.vpc.private_subnets
  vpc_id = module.vpc.vpc_id
  
  tags = {
    environment = var.env_prefix
    application = "myapp_project"
  }

  cluster_addons = {
    aws-ebs-csi-driver = {}
  }

/*configuration for worker nodes*/

/*
  eks_managed_node_groups = {
    node_group = {
      min_size     = 1
      max_size     = 3
      desired_size = 3
      instance_types = [var.instance_type]
      node_group_name = var.env_prefix
      use_custom_templates = false
      tags = {
        Name = "${var.env_prefix}"
      }
      iam_role_additional_policies = {
        AmazonEBSCSIDriverPolicy = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
      }
    }
  }
  fargate_profiles = {
    profile = {
      name = "${var.env_prefix}-fargate-profile"
      selectors = [
        {
            namespace = "my-app"
        }
      ]
    }
  }
}
*/
