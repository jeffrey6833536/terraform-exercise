variable vpc_cidr_block {
    default = "10.0.0.0/16"
}

variable private_subnet_cidr_blocks {
    default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable public_subnet_cidr_blocks {
    default = ["10.0.7.0/24", "10.0.8.0/24", "10.0.9.0/24"]
}

variable region {
    default = "eu-central-1"
}

variable eks_cluster_name {
    default = "myapp_cluster"
}

variable env_prefix {
    default = "test"
}

variable k8_version {
    default = "1.29"
}

variable instance_type {
    default = "t3.small"
}