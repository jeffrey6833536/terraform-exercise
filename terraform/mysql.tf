/*

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_name
  depends_on =[module.eks]
}

data "aws_eks_cluster_auth" "my_cluster_auth" {
  name = module.eks.cluster_name
  depends_on =[module.eks]
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.my_cluster_auth.token
}

provider "helm" {
    kubernetes {
        host                   = data.aws_eks_cluster.cluster.endpoint
        cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
        token                  = data.aws_eks_cluster_auth.my_cluster_auth.token
    }
}


resource "helm_release" "mysql" {
    name       = "mysql"
    repository = "https://charts.bitnami.com/bitnami"
    chart      = "mysql"
    version    = "9.14.0"
    timeout    = "1000"

    values = [
        "${file("mysql-values.yaml")}"
    ]
}
*/